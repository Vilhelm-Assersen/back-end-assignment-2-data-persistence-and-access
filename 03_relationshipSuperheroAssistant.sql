use SuperheroesDb;

ALTER TABLE	Assistant
	ADD SuperheroId int foreign key REFERENCES Superhero(ID);