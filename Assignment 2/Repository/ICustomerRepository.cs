﻿using Assignment_2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.Repository
{
    public interface ICustomerRepository
    {
        public Customer GetCustomerById(int id); //Used to get a single album
        public List<Customer> GetCustomerByFirstName(string name); //Used to get a single album
        public List<Customer> GetAllCustomer(); //Used to get all Albums in form of a list
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerCountry> GetCustomersByCountry();
        public List<CustomerInvoice> GetCustomersByInvoice();
        public List<CustomerGenre> GetCustomersByGenre(int id);
    }
}
