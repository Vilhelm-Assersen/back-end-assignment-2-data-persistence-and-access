﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.Repository
{
    public class ConnectionHelper
    {
        public static string GetConnectionString()
        {
            //ConnectionStringBuilder
            SqlConnectionStringBuilder connectStringBuilder= new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "N-NO-01-03-3958\\SQLEXPRESS"; //Connecting to ssms
            connectStringBuilder.InitialCatalog = "Chinook"; //Chosen database
            connectStringBuilder.IntegratedSecurity = true;
            return connectStringBuilder.ConnectionString;
        }
    }
}
