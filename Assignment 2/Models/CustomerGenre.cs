﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.Models
{
    public class CustomerGenre
    {
        public string GenreName { get; set; }
        public int GenreId { get; set; }
    }
}
