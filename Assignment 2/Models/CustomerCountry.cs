﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.Models
{
    public class CustomerCountry
    {
        public string CountryName { get; set; }
        public int NumberOfCustomer { get; set; }
    }
}
