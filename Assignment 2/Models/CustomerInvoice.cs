﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.Models
{
    public class CustomerInvoice
    {
        public int CustomerId { get; set; }
        public decimal InvoiceTotal { get; set; }
    }
}
