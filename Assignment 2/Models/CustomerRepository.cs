﻿using Assignment_2.Repository;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Assignment_2.Models
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomer()
        {
            // Getting al the albums from the database
            List<Customer> customerList = new List<Customer>();

            //Use the Select statement
            string sql = "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email, SupportRepId FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Company = reader.IsDBNull(3) ? " no data " : reader.GetString(3);
                                temp.Address = reader.IsDBNull(4) ? " no data " : reader.GetString(4);
                                temp.City = reader.IsDBNull(5) ? " no data " : reader.GetString(5);
                                temp.State = reader.IsDBNull(6) ? " no data " : reader.GetString(6);
                                temp.Country = reader.IsDBNull(7) ? " no data " : reader.GetString(7);
                                temp.PostalCode = reader.IsDBNull(8) ? " no data " : reader.GetString(8);
                                temp.Phone = reader.IsDBNull(9) ? " no data " : reader.GetString(9);
                                temp.Fax = reader.IsDBNull(10) ? " no data " : reader.GetString(10);
                                temp.Email = reader.GetString(11);
                                temp.SupportRepId = reader.GetInt32(12);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return customerList;
        }

        public Customer GetCustomerById(int id)
        {
            // Getting al the albums from the database
            Customer customer = new Customer();

            //Use the Select statement
            string sql = "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email, SupportRepId FROM Customer " + 
                "WHERE CustomerId=@CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                //All the coloums that doesn't contain "NOT NULL" in the database are checked. Data like "CustomerId" cannot be set to null 
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Company = reader.IsDBNull(3) ? " no data " : reader.GetString(3);
                                customer.Address = reader.IsDBNull(4) ? " no data " : reader.GetString(4);
                                customer.City = reader.IsDBNull(5) ? " no data " : reader.GetString(5);
                                customer.State = reader.IsDBNull(6) ? " no data " : reader.GetString(6);
                                customer.Country = reader.IsDBNull(7) ? " no data " : reader.GetString(7);
                                customer.PostalCode = reader.IsDBNull(8) ? " no data " : reader.GetString(8);
                                customer.Phone = reader.IsDBNull(9) ? " no data " : reader.GetString(9);
                                customer.Fax = reader.IsDBNull(10) ? " no data " : reader.GetString(10);
                                customer.Email = reader.GetString(11);
                                customer.SupportRepId = reader.GetInt32(12);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

            }
            return customer;
        }

        public List<Customer> GetCustomerByFirstName(string name)
        {
            // Getting all the customers that share the given name/partial name
            List<Customer> customerList = new List<Customer>();

            //Use the Select statement
            string sql = "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email, SupportRepId FROM Customer " +
                "WHERE FirstName LIKE @FirstName";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", "%" + name + "%");
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Company = reader.IsDBNull(3) ? " no data " : reader.GetString(3);
                                temp.Address = reader.IsDBNull(4) ? " no data " : reader.GetString(4);
                                temp.City = reader.IsDBNull(5) ? " no data " : reader.GetString(5);
                                temp.State = reader.IsDBNull(6) ? " no data " : reader.GetString(6);
                                temp.Country = reader.IsDBNull(7) ? " no data " : reader.GetString(7);
                                temp.PostalCode = reader.IsDBNull(8) ? " no data " : reader.GetString(8);
                                temp.Phone = reader.IsDBNull(9) ? " no data " : reader.GetString(9);
                                temp.Fax = reader.IsDBNull(10) ? " no data " : reader.GetString(10);
                                temp.Email = reader.GetString(11);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }

        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // CustomerId is not needed since it is added automatically (auto-incremented)
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }

        public bool UpdateCustomer(Customer customer)
        {
            {
                bool success = false;
                string sql = "UPDATE Customer " +
                    "SET FirstName = @FirstName, " +
                    "LastName = @LastName, " +
                    "Company = @Company, " +
                    "Address = @Address, " +
                    "City = @City, " +
                    "State = @State, " +
                    "Country = @Country, " +
                    "PostalCode = @PostalCode, " +
                    "Phone = @Phone, " +
                    "Fax = @Fax, " +
                    "Email = @Email " +
                    "WHERE CustomerId = 1"; //Tried to use @CustomerId instead of just 1, but wouldn't let me

                try
                {
                    using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                    {
                        conn.Open();
                        // Make a command
                        using (SqlCommand cmd = new SqlCommand(sql, conn))
                        {
                            // CustomerId is not needed since it is added automatically (auto-incremented)
                            cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                            cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                            cmd.Parameters.AddWithValue("@Company", customer.Company);
                            cmd.Parameters.AddWithValue("@Address", customer.Address);
                            cmd.Parameters.AddWithValue("@City", customer.City);
                            cmd.Parameters.AddWithValue("@State", customer.State);
                            cmd.Parameters.AddWithValue("@Country", customer.Country);
                            cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                            cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                            cmd.Parameters.AddWithValue("@Fax", customer.Fax);
                            cmd.Parameters.AddWithValue("@Email", customer.Email);
                            cmd.Parameters.AddWithValue("@SupportRepId", customer.SupportRepId);
                            success = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                return success;
            }

        }

        public List<CustomerCountry> GetCustomersByCountry()
        {
            // Getting al the albums from the database
            List<CustomerCountry> customerList = new List<CustomerCountry>();

            //Use the Select statement
            string sql = "SELECT Country, COUNT (*) AS Number " +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY Number DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerCountry temp = new CustomerCountry();
                                temp.CountryName = reader.IsDBNull(0) ? " no data " : reader.GetString(0);
                                temp.NumberOfCustomer = reader.GetInt32(1);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }

        public List<CustomerInvoice> GetCustomersByInvoice()
        {
            // Getting al the albums from the database
            List<CustomerInvoice> customerList = new List<CustomerInvoice>();

            //Use the Select statement
            string sql = "SELECT CustomerId, sum(Total) as TotalInvoice " +
                "FROM Invoice " +
                "group by CustomerId " +
                "order by TotalInvoice desc;";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerInvoice temp = new CustomerInvoice();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.InvoiceTotal = reader.GetDecimal(1);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }

        public List<CustomerGenre> GetCustomersByGenre(int id)
        {
            // Getting al the albums from the database
            List<CustomerGenre> customerList = new List<CustomerGenre>();

            //Use the Select statement
            string sql = "SELECT TOP 1 WITH TIES Genre.Name, COUNT (genre.genreid) AS number " +
                "FROM Invoice " +
                "INNER JOIN invoiceLine ON Invoice.InvoiceId = invoiceLine.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                "WHERE Invoice.CustomerId = @CustomerId " +
                "GROUP BY Genre.Name " +
                "ORDER BY number";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerGenre temp = new CustomerGenre();
                                temp.GenreName = reader.GetString(0);
                                temp.GenreId = reader.GetInt32(1);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }
    }
}
