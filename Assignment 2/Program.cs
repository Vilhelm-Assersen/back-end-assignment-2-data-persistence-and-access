﻿using Assignment_2.Models;
using Assignment_2.Repository;
using Microsoft.Data.SqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace Assignment_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();

            Customer test = new Customer() 
            {
                FirstName = "Akinator",
                LastName = "Boopydoop",
                Company = "Experis Academy",
                Address = "at home",
                City = "Stavanger",
                State = "Rogaland",
                Country = "Norway",
                PostalCode = "4344",
                Phone = "12345678",
                Fax = "123",
                Email = "aøkfn@fuckyou.cunt",
                SupportRepId = 12
            };

            //TestSelectAll(repository);
            //Examples you can use to check the methods are working correctly
            //PrintCustomers(repository.GetAllCustomer()); //Print all customers
            //PrintCustomer(repository.GetCustomerById(1)); //Print customer by ID
            //PrintCustomers(repository.GetCustomerByFirstName("Ka")); //Print customer(s) by name or partial of a name 

            //Adds new customer to the database
            
            /*
            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("Yay it worked!");
                PrintCustomers(repository.GetCustomerByFirstName("Akinator"));
            }
            else
            {
                Console.WriteLine("Fuck this shit ");
            }
            */

            //Update Customer nr 1. Coulnd't get it to select a specific customer so I just set CustomerId to 1 instead of @CustomerId
            /*
            if (repository.UpdateCustomer(test))
            {
                Console.WriteLine("Yay it worked!");
                PrintCustomers(repository.GetCustomerByFirstName("Akinator"));
            }
            else
            {
                Console.WriteLine("Fuck this shit ");
            }
            */

            //PrintCustomersCountry(repository.GetCustomersByCountry()); //Prints the number of customers per country in descending order
            //PrintCustomersInvoice(repository.GetCustomersByInvoice()); //Prints the biggest spender by total invoices
            //PrintCustomersGenre(repository.GetCustomersByGenre(2)); //Prints the customers most populare genre



        }
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- " +
                $"{customer.CustomerId} " +
                $"{customer.FirstName} " +
                $"{customer.LastName} " +
                $"{customer.Company} " +
                $"{customer.Address} " +
                $"{customer.City} " +
                $"{customer.State} " +
                $"{customer.Country} " +
                $"{customer.PostalCode} " +
                $"{customer.Phone} " +
                $"{customer.Fax} " +
                $"{customer.Email} " +
                $"{customer.SupportRepId} ---");
        }

        //Prints the number of customers per country in descending order
        static void PrintCustomersCountry(IEnumerable<CustomerCountry> customersCountry)
        {
            foreach (CustomerCountry customerCountry in customersCountry)
            {
                PrintCustomerCountry(customerCountry);
            }
        }

        //Prints the number of customers per country in descending order
        static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"{customerCountry.CountryName}: {customerCountry.NumberOfCustomer}");
        }

        //Prints the total of the invoices per customer in descending order
        static void PrintCustomersInvoice(IEnumerable<CustomerInvoice> customersInvoices)
        {
            foreach (CustomerInvoice customerInvoice in customersInvoices)
            {
                PrintCustomerInvoice(customerInvoice);
            }
        }

        //Prints the number of customers per country in descending order
        static void PrintCustomerInvoice(CustomerInvoice customerInvoices)
        {
            Console.WriteLine($"Customer ID {customerInvoices.CustomerId}: {customerInvoices.InvoiceTotal}");
        }

        //Prints the most populare genre for the given customer
        static void PrintCustomersGenre(IEnumerable<CustomerGenre> customersGenre)
        {
            foreach (CustomerGenre customerGenre in customersGenre)
            {
                PrintCustomerGenre(customerGenre);
            }
        }

        //Prints the number of customers per country in descending order
        static void PrintCustomerGenre(CustomerGenre customerGenres)
        {
            Console.WriteLine($"{customerGenres.GenreName}: {customerGenres.GenreId}");
        }

    }
}
