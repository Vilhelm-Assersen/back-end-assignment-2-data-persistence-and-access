create table Superheropowers(
	ID int identity(1,1) primary key
);

alter table Superheropowers
	add HeroId int foreign key references Superhero(ID);
	
alter table Superheropowers
	add PowerId int foreign key references Power(ID);