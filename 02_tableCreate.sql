CREATE TABLE Superhero (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(255),
	Alias varchar(255),
	Origin varchar(255)
);	

CREATE TABLE Assistant(
	ID int identity(1,1) primary key,
	Name varchar(255)
);

CREATE TABLE Power (
	ID int identity(1,1) primary key,
	Name varchar(255),
	Description varchar(255)
);

