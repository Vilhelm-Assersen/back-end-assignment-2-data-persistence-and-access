# Creating a WebAPI and database
By using C# in Visual Studio we, me and Vilhelm Assersen, created a webAPI that uses a database containting franchises, movies and characters that are connected. They're either connected by one-to-many or many-to-many. This program allows the webAPI to communicate and manipulate the database by using the C# program as a middleman. 

## Why did we create this?
In order to complete assignment 2 we created this C# program. Here we learned how to create both a webAPI and a database with relationships built in. We learned how to make a program that manipulates the database externally. Last, but not least we learned how to manipulate the database through the webAPI. 

## How to run the program
After downloading the files the program is good to go. Simply by running the program you open a swagger page. Here you get up all the options for manipulation the database. The type of commans are "Get: prints an instance of the entity", "Put: Updates an instance of the entity", "Post: Creates an instance of the entity", "Delete: deletes an instance of the entity"
